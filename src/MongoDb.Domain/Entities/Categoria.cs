﻿using MongoDb.Domain.Entities.Shared;

namespace MongoDb.Domain.Entities
{
    public class Categoria : Entity
    {
        public string Nome { get; set; }

        public ICollection<Produto> Produtos { get; private set; }

        public Categoria(Guid id, string nome)
        {
            Id = id;
            Nome = nome;
        }
    }
}
