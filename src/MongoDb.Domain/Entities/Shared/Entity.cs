﻿namespace MongoDb.Domain.Entities.Shared
{
    public abstract class Entity
    {
        public Guid Id { get; protected set; }
    }
}
