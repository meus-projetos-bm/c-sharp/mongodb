﻿using MongoDb.Domain.Entities;
using MongoDB.Driver;

namespace MongoDb.Data.Context
{
    public interface IAppDbContext
    {
        IMongoCollection<Produto> Produtos { get; }
        IMongoCollection<Categoria> Categorias { get; }
    }
}
