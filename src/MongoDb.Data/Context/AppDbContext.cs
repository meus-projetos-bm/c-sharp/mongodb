﻿using Microsoft.Extensions.Configuration;
using MongoDb.Domain.Entities;
using MongoDB.Driver;

namespace MongoDb.Data.Context
{
    public class AppDbContext : IAppDbContext
    {
        public AppDbContext(IConfiguration configuration)
        {
            var client = new MongoClient(configuration.GetSection("DATABASESETTINGS:ConnectionString").Value);
            var database = client.GetDatabase(configuration.GetSection("DATABASESETTINGS:DatabaseName").Value);
            Produtos = database.GetCollection<Produto>("");
            Categorias = database.GetCollection<Categoria>("");
        }

        public IMongoCollection<Produto> Produtos { get; }
        public IMongoCollection<Categoria> Categorias { get; }
    }
}
